# 😀😀 Sublime Hawthorn Compiler 😀😀

## Difference of Syntax between 2023BUAA_Cmp & 2024Cmp_Contest

- Add support for float value 
- Add I/O functions & time functions
- Add WhileStmt & Remove ForStmt

## Optimization on Intermediate Representation
- Mem2Reg
- GVN-GCM
- Dead Code Elimination
- Function Inline 
- Tail Call Elimination
- Constant Folding & Operational Strength Reduce
- Global Var Localization & Array To Var
- Loop Strength Reduce 
- Loop Unroll
- Memcpy Optimize
- Inductive Value Reuse
- Inductive Value Evaluation & Scalar Evolution

## Compiler Options
- "-S" Compiler will generate Assembly instead of Binary Code (We cannot generate Binary Code at all though).
- "-o" The path after this param will be Assembly output.
- "-O1" Allow compiler to optimize
- "output_llvm" Allow the LLVM output for debugging.  
example: `compiler testcase.sysy -S -o testcase.s -O1 -output_llvm`