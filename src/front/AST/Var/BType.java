package src.front.AST.Var;

import src.front.AST.Node;
import src.front.AST.TokenNode;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// BType ==> 'int' | 'float'
public class BType extends Node {
    public BType(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public boolean isInt() {
        return ((TokenNode) children.get(0)).getToken().getValue().equals("int");
    }
}
