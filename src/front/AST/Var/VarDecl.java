package src.front.AST.Var;

import src.mid.IntermediatePresentation.Value;
import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// VarDecl ==> BType VarDef { ',' VarDef } ';'
public class VarDecl extends Node {

    public VarDecl(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Value toIR() {
        boolean isInt = ((BType) children.get(0)).isInt();

        for (Node child : children) {
            if (child instanceof VarDef varDef) {
                varDef.toIR(isInt);
            }
        }
        return null;
    }

}
