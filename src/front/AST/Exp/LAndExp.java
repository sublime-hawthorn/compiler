package src.front.AST.Exp;

import src.front.AST.Node;
import src.mid.IntermediatePresentation.BasicBlock;
import src.mid.IntermediatePresentation.Instruction.Br;
import src.mid.IntermediatePresentation.Instruction.ZextTo;
import src.mid.IntermediatePresentation.Value;
import src.mid.IntermediatePresentation.ValueType;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

/*
 LAndExp ==> EqExp { '&&' EqExp }
 */
public class LAndExp extends Node {

    public LAndExp(SyntaxType SType, ArrayList<Node> children) {
        super(SType, children);
    }

    public void condToIR(BasicBlock ifTrue, BasicBlock ifFalse) {
        Value cond;

        if (children.size() == 1) {
            cond = children.get(0).toIR();
            new Br(cond.toI1(), ifTrue, ifFalse);
        } else {
            for (Node child : children) {
                if (child instanceof EqExp eqExp) {
                    BasicBlock curBB = irManager.getCurBlock();
                    BasicBlock trueThen = child.equals(children.get(children.size() - 1)) ?
                            ifTrue : new BasicBlock();
                    irManager.setCurBlock(curBB);

                    cond = eqExp.toIR();
                    new Br(cond.toI1(), trueThen, ifFalse);
                    irManager.setCurBlock(trueThen);
                }
            }
        }
    }
}
