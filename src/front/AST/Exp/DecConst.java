package src.front.AST.Exp;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

public class DecConst extends NumberNode {
    public DecConst(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Number evaluate() {
        return Integer.parseInt(getTokenValue());
    }

}
