package src.front.AST.Exp;

import src.mid.IntermediatePresentation.ConstNumber;
import src.mid.IntermediatePresentation.Value;
import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// Number ==> HEXCON,
//    OCTCON,
//    DECCON,
//    HEXFCON,
//    DECFCON
public class NumberNode extends Node {

    public NumberNode(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Number evaluate() {
        return children.get(0).evaluate();
    }

    public Value toIR() {
        return new ConstNumber(evaluate());
    }

}
