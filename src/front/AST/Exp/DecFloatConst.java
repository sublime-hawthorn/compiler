package src.front.AST.Exp;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

public class DecFloatConst extends NumberNode {
    public DecFloatConst(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Number evaluate() {
        return Float.parseFloat(getTokenValue());
    }
}
