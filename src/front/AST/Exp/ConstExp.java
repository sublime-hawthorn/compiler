package src.front.AST.Exp;

import src.front.AST.Node;
import src.mid.IntermediatePresentation.ConstNumber;
import src.mid.IntermediatePresentation.Value;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

/*
 ConstExp ==> AddExp
 */
public class ConstExp extends Node {

    public ConstExp(SyntaxType SType, ArrayList<Node> children) {
        super(SType, children);
    }

    public Value toIR() {
        try {
            java.lang.Number val = evaluate();
            return new ConstNumber(val);
        } catch (NullPointerException e) {
            return children.get(0).toIR();
        }
    }

    public Number evaluate() {
        return children.get(0).evaluate();
    }

}
