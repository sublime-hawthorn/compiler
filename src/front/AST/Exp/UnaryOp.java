package src.front.AST.Exp;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// UnaryOp ==> '+' | '-' | '!'
public class UnaryOp extends Node {

    public UnaryOp(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

}
