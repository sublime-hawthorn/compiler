package src.front.AST.Func;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// FuncRParams ==> Exp { ',' Exp }
public class FuncRParams extends Node {

    public FuncRParams(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

}
