package src.front.AST.Func;

import src.mid.IntermediatePresentation.Function.Param;
import src.mid.IntermediatePresentation.Value;
import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// FuncFParams ==> FuncFParam { ',' FuncFParam }
public class FuncFParams extends Node {

    public FuncFParams(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Value toIR() {
        Param param = new Param();
        for (Node child : children) {
            if (child instanceof FuncFParam) {
                param.addParam(child.toIR());
            }
        }
        return param;
    }

}
