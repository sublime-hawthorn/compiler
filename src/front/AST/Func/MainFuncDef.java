package src.front.AST.Func;

import src.mid.IntermediatePresentation.Function.MainFunction;
import src.mid.IntermediatePresentation.Value;
import src.mid.IntermediatePresentation.ValueType;
import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// MainFuncDef ==> 'int' 'main' '(' ')' Block
public class MainFuncDef extends Node {

    public MainFuncDef(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Value toIR() {
        MainFunction mainFunction = new MainFunction();
        symbolTableManager.funcDecl(ValueType.I32, "@main");
        symbolTableManager.enterBlock();
        super.toIR();
        symbolTableManager.funcDeclEnd();
        return mainFunction;
    }

}
