package src.front.AST.Func;

import src.mid.IntermediatePresentation.ValueType;
import src.front.AST.Node;
import src.front.AST.TokenNode;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// FuncType ==> 'void' | 'int' | 'float'
public class FuncType extends Node {

    public FuncType(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public ValueType getRetType() {
        String retType = ((TokenNode) children.get(0)).getToken().getValue();
        if (retType.equals("void")) {
            return ValueType.NULL;
        } else if (retType.equals("int")) {
            return ValueType.I32;
        } else {
            return ValueType.FLT;
        }
    }

}
