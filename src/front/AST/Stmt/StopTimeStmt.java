package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// StopTimeStmt ==> 'stoptime' '(' ')';
public class StopTimeStmt extends ExpStmt {
    public StopTimeStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
