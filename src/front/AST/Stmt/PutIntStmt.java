package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// PutIntStmt ==> 'putint' '(' Exp ')' ';'
public class PutIntStmt extends ExpStmt {
    public PutIntStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
