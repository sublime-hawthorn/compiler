package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// Stmt ==> AssignStmt | ExpStmt | BlockStmt | IfStmt | WhileStmt | BreakStmt | ReturnStmt | GetIntStmt
public class Stmt extends Node {

    public Stmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

}
