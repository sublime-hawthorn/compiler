package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// GetIntStmt ==> LVal '=' 'getint' '(' ')' ';'
public class GetIntStmt extends AssignStmt {

    public GetIntStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

}
