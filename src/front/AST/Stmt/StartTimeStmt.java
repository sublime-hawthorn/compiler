package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// StartTimeStmt ==> 'starttime' '(' ')' ';'
public class StartTimeStmt extends ExpStmt{
    public StartTimeStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
