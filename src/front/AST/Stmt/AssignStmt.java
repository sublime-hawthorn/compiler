package src.front.AST.Stmt;

import src.front.AST.Node;
import src.mid.IntermediatePresentation.ConstNumber;
import src.mid.IntermediatePresentation.Instruction.GetElementPtr;
import src.mid.IntermediatePresentation.Instruction.Store;
import src.mid.IntermediatePresentation.Value;
import src.mid.IntermediatePresentation.ValueType;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// AssignStmt ==> LVal '=' Exp ';'
public class AssignStmt extends Stmt {

    public AssignStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Value toIR() {
        Value lval = children.get(0).toIR();
        Value exp = children.get(2).toIR();

        if (!(lval instanceof GetElementPtr)) {
            if (exp instanceof ConstNumber n) {
                symbolTableManager.setVal(getTokenValue(), lval.isFloat() ? n.getVal().floatValue() :
                        n.getVal().intValue());
            } else {
                symbolTableManager.setVal(getTokenValue(), null);
            }
        }
        boolean isInt = (lval.getRefType() == ValueType.I32);
        return new Store(exp.withType(isInt), lval);
    }
}
