package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// PutArrayStmt ==> 'putarray' '(' Exp ',' Exp { ',' Exp} ')' ';'
public class PutArrayStmt extends ExpStmt {
    public PutArrayStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
