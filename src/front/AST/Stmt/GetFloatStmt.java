package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// GetFloatStmt ==> LVal '=' 'getfloat' '(' ')' ';'
public class GetFloatStmt extends AssignStmt {

    public GetFloatStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
