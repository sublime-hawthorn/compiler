package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// GetFArrayStmt ==> LVal '=' 'getfarray' '(' Exp ')' ';'
public class GetFArrayStmt extends AssignStmt {

    public GetFArrayStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
