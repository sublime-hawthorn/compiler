package src.front.AST.Stmt;

import src.front.AST.Node;
import src.mid.IntermediatePresentation.Instruction.Br;
import src.mid.IntermediatePresentation.Value;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// ContinueStmt ==> 'continue' ';'
public class ContinueStmt extends Stmt {

    public ContinueStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Value toIR() {
        return new Br(irManager.getContinueTo());
    }

}
