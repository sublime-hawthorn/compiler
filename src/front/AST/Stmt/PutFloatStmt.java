package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// PutFloatStmt ==> 'putfloat' '(' Exp ')' ';'
public class PutFloatStmt extends ExpStmt {
    public PutFloatStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
