package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// ExpStmt ==> [ Exp ] ';'
public class ExpStmt extends Stmt {
    public ExpStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

}
