package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// PutChStmt ==> 'putch' '(' Exp ')' ';'
public class PutChStmt extends ExpStmt {
    public PutChStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
