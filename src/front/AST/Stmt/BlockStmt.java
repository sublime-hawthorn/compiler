package src.front.AST.Stmt;

import src.front.AST.Node;
import src.mid.IntermediatePresentation.Value;
import src.mid.SymbolTable.SymbolTableManager;
import src.utils.type.SyntaxType;

import java.io.PipedOutputStream;
import java.util.ArrayList;

// BlockStmt ==> Block
public class BlockStmt extends Stmt {

    public BlockStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }

    public Value toIR() {
        SymbolTableManager.getInstance().enterBlock();
        for (Node child : children) {
            child.toIR();
        }
        SymbolTableManager.getInstance().exitBlock();
        return null;
    }

}
