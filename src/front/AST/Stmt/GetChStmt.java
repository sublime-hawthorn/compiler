package src.front.AST.Stmt;

import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// GetChStmt ==> LVal '=' 'getch' '(' ')' ';'
public class GetChStmt extends AssignStmt{
    public GetChStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}
