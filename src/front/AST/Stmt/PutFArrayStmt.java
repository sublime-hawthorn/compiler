package src.front.AST.Stmt;

import src.front.AST.Exp.Exp;
import src.front.AST.Node;
import src.utils.type.SyntaxType;

import java.util.ArrayList;

// PutFArrayStmt ==> 'putfarray' '(' Exp ',' Exp { ',' Exp } ')' ';'
public class PutFArrayStmt extends ExpStmt {
    public PutFArrayStmt(SyntaxType sType, ArrayList<Node> children) {
        super(sType, children);
    }
}