package src;

import src.backend.Backend;
import src.backend.tools.BackModule;
import src.mid.IntermediatePresentation.IRManager;
import src.mid.IntermediatePresentation.Module;
import src.front.AST.Node;
import src.front.lexer.Lexer;
import src.front.parser.Parser;
import src.mid.Optimizer.Optimizer;
import src.utils.Config;
import src.utils.tools.Timer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PushbackReader;

import static java.lang.System.exit;

public class Compiler {
    public static void main(String[] args) throws IOException {
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (arg.equals("-O1")) {
                Config.opt = true;
            } else if (arg.contains("-output_llvm")) {
                Config.output_llvm = true;
            } else if (arg.equals("-o")) {
                Config.output = args[i + 1];
                i++;
            } else if (arg.equals("-S")) {
                continue;
            } else {
                Config.input = arg;
            }
        }

        File inFile = new File(Config.input);
        PushbackReader inputStream = new PushbackReader(new FileReader(inFile));

        Timer.INSTANCE.start();

        // front
        Lexer lexer = new Lexer(inputStream);
        Parser parser = new Parser(lexer.getTokenStream());
        Node compUnit = parser.parseCompUnit();

        // mid
        compUnit.toIR();
        Module module = IRManager.getModule();
        output("ir_un_opt.ll", module);
        System.out.println("opt");
        // mid optimize
        if (Config.opt) {
            try {
                Optimizer.instance().optimize();
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Compiler.output("if err.ll", IRManager.getModule());
                } catch (Exception oe) {
                    System.out.println("output failed");
                }
                exit(-1);
            }
        } else if (module.getDecledFunctions().stream().allMatch(f -> f.getBlocks().size() <= 3000)) {
            // 否则会tle...
            Optimizer.instance().backendBasicOpt();
        } else {
            Optimizer.instance().mem2regOnly();
        }
        System.out.println("back");
        Backend backend = new Backend(module);
        BackModule backModule = backend.run();
        System.out.println("opt");
        if (Config.opt) {
            backModule.optimize();
        }
        outputBack(Config.output, backModule);
    }

    public static void output(String path, Object o) {
        if (o.equals(IRManager.getModule()) && !Config.output_llvm) {
            return;
        }
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(path));
            bw.write(o.toString());
            bw.flush();
            bw.close();
        } catch (IOException e) {
            System.out.println("output failed");
        }
    }

    public static void outputBack(String path, Object o) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(path));
        bw.write(o.toString());
        bw.flush();
        bw.close();
    }
}
