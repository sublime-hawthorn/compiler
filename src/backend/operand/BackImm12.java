package src.backend.operand;

public class BackImm12 extends BackOperand {
    private int imm;

    public BackImm12(int imm) {
        this.imm = imm;
    }

    @Override
    public String toString() {
        return String.valueOf(imm);
    }

    public int getImm() {
        return imm;
    }
}
