package src.backend.Pass;

import src.backend.instructions.BackBranch;
import src.backend.instructions.BackCall;
import src.backend.instructions.BackInstruction;
import src.backend.instructions.BackLoad;
import src.backend.instructions.BackMov;
import src.backend.instructions.BackRet;
import src.backend.instructions.BackStore;
import src.backend.operand.BackIReg;
import src.backend.operand.BackImm;
import src.backend.operand.BackImm12;
import src.backend.operand.BackOperand;
import src.backend.tools.BackBlock;
import src.backend.tools.BackFunction;
import src.backend.tools.BackModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class InstructionMerge {
    private final BackModule backModule;

    public InstructionMerge(BackModule backModule) {
        this.backModule = backModule;
    }

    public boolean optimize() {
        boolean flag = false;
//        flag = peepHole();
        flag |= deadRegElim();
        flag |= loadStoreMerge();
        return flag;
    }

//    private boolean peepHole() {
//        boolean flag = false;
//
//        return flag;
//    }

    private boolean deadRegElim() {
        boolean flag = false;
        for (BackFunction backFunction : backModule.getFunctions()) {
            HashMap<BackOperand, BackInstruction> dstToInstr = new HashMap<>();
            HashSet<BackBlock> continusBlocks = new HashSet<>();
            HashSet<BackInstruction> deadInstructions = new HashSet<>();

            for (BackBlock backBlock : backFunction.getBackBlocks()) {
                continusBlocks.add(backBlock);
                for (BackInstruction backInstruction : backBlock.getBackInstructions()) {
                    if (backInstruction instanceof BackBranch || backInstruction instanceof BackRet) {
                        for (BackBlock block : continusBlocks) {
                            block.getBackInstructions().removeAll(deadInstructions);
                        }
                        deadInstructions.clear();
                        dstToInstr.clear();
                        continusBlocks.clear();
                        continusBlocks.add(backBlock);
                    }

                    for (BackOperand backOperand : backInstruction.getOperands()) {
                        if (!(backOperand instanceof BackImm || backOperand instanceof BackImm12)) {
                            dstToInstr.remove(backOperand);
                        }
                    }

                    if (backInstruction instanceof BackCall) {
                        // 设置的所有参数寄存器是有效的，所有其他临时寄存器的赋值是无效的
                        for (BackOperand backOperand : new HashSet<>(dstToInstr.keySet())) {
                            if (backOperand instanceof BackIReg reg) {
                                if (reg.isParamRegs()) {
                                    dstToInstr.remove(reg);
                                } else if (reg.isTempRegs()) {
                                    deadInstructions.add(dstToInstr.get(reg));
                                    dstToInstr.remove(reg);
                                    flag = true;
                                }
                            }
                        }
                        continue;
                    }


                    BackOperand dst = backInstruction.getDst();
                    if (dst != null) {
                        if (dstToInstr.containsKey(dst)) {
                            deadInstructions.add(dstToInstr.get(dst));
                            flag = true;
                        } else if (backInstruction instanceof BackMov mov && dstToInstr.containsKey(mov.getSrc())) {
                            // r0 <- a ... mv r1, r0 若过程中没有用到r1，则可以将此指令以及期间所有指令的r0转为r1，然后删去此mv
                            BackOperand mvSrc = mov.getSrc();
                            ArrayList<BackInstruction> instructions = new ArrayList<>();
                            BackInstruction src = dstToInstr.get(mvSrc);
                            boolean inRange = false;
                            for (BackBlock block : backFunction.getBackBlocks()) {
                                for (BackInstruction instruction : block.getBackInstructions()) {
                                    if (instruction.equals(src)) {
                                        inRange = true;
                                    }
                                    if (inRange) {
                                        instructions.add(instruction);
                                    }
                                    if (instruction.equals(mov)) {
                                        break;
                                    }
                                }
                            }
                            if (instructions.stream().noneMatch(
                                    i -> i.getOperands().contains(dst) || dst.equals(i.getDst()))) {
                                // 不考虑call，因为上面已经处理过了
                                for (BackInstruction instr : instructions) {
                                    instr.replaceOperand(mvSrc, dst);
                                }
                                deadInstructions.add(mov);
                                dstToInstr.put(dst, dstToInstr.get(mvSrc));
                                dstToInstr.remove(mvSrc);
                                continue;
                            }
                        }
                        dstToInstr.put(dst, backInstruction);
                    }
                }
            }
        }
        return flag;
    }

    private boolean loadStoreMerge() {
        // 从store开始，只要src未被破坏，则可以将后续所有的load改写为move
        // 多个未被load或call，即被覆盖的store指令可以只取最后一个
        // 考虑的地址是<off, reg>的二元组
        boolean flag = false;
        for (BackFunction backFunction : backModule.getFunctions()) {
            HashMap<BackAddr, BackStore> addrToStore = new HashMap<>();
            HashMap<BackAddr, BackOperand> addrToSrc = new HashMap<>();
            HashSet<BackBlock> continusBlocks = new HashSet<>();
            HashSet<BackInstruction> deadInstructions = new HashSet<>();

            for (BackBlock backBlock : backFunction.getBackBlocks()) {
                continusBlocks.add(backBlock);
                for (BackInstruction backInstruction : new ArrayList<>(backBlock.getBackInstructions())) {
                    if (backInstruction instanceof BackBranch || backInstruction instanceof BackRet ||
                            backInstruction instanceof BackCall) {
                        for (BackBlock block : continusBlocks) {
                            block.getBackInstructions().removeAll(deadInstructions);
                        }
                        deadInstructions.clear();
                        addrToStore.clear();
                        addrToSrc.clear();
                        continusBlocks.clear();
                        continusBlocks.add(backBlock);
                    }


                    BackOperand dst = backInstruction.getDst();
                    if (dst instanceof BackIReg reg) {
                        if (backInstruction instanceof BackLoad backLoad && backLoad.getOffset() != null) {
                            BackAddr backAddr = new BackAddr(backLoad.getOffset(), reg);
                            if (addrToSrc.containsKey(backAddr)) {
                                backBlock.getBackInstructions().set(backBlock.getBackInstructions().indexOf(backLoad),
                                        new BackMov(reg, addrToSrc.get(backAddr)));
                                flag = true;
                            }
                            addrToStore.remove(backAddr);
                        }

                        // 被破坏，相应使用该寄存器的所有地址都无效了
                        HashSet<BackAddr> uncertainAddrs = new HashSet<>();
                        for (BackAddr addr : addrToStore.keySet()) {
                            if (addr.reg.equals(reg)) {
                                uncertainAddrs.add(addr);
                            }
                        }
                        uncertainAddrs.forEach(addrToStore::remove);
                        uncertainAddrs.forEach(addrToSrc::remove);

                        // 也不能作为src代替了
                        HashSet<BackAddr> uncertainSrcs = new HashSet<>();
                        for (BackAddr addr : addrToSrc.keySet()) {
                            if (addrToSrc.get(addr).equals(reg)) {
                                uncertainSrcs.add(addr);
                            }
                        }
                        uncertainSrcs.forEach(addrToSrc::remove);
                    }

                    // 如果是sw，且addr相同，则可以删掉了
                    if (backInstruction instanceof BackStore backStore && backStore.getOffset() != null) {
                        BackAddr backAddr = new BackAddr(backStore.getOffset(), backStore.getBase());
                        if (addrToStore.containsKey(backAddr)) {
                            deadInstructions.add(addrToStore.get(backAddr));
                            flag = true;
                            addrToStore.put(backAddr, backStore);
                        }
                        addrToSrc.put(backAddr, backStore.getSrc());
                    }
                }
            }
        }
        return flag;
    }

    private class BackAddr {
        public final int offset;
        public final BackIReg reg;

        public BackAddr(int offset, BackIReg reg) {
            this.offset = offset;
            this.reg = reg;
        }

        public boolean equals(Object o) {
            if (!(o instanceof BackAddr addr)) {
                return false;
            }
            return offset == addr.offset && reg.equals(addr.reg);
        }
    }
}
