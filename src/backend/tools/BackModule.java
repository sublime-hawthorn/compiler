package src.backend.tools;

import src.backend.Pass.InstructionMerge;
import src.backend.Pass.Straighten;
import src.mid.IntermediatePresentation.BasicBlock;

import java.util.ArrayList;
import java.util.HashMap;

public class BackModule {
    private ArrayList<BackFunction> backFunctions;
    private ArrayList<BackGlobalDecl> backGlobalDecls;
    public HashMap<BasicBlock, BackBlock> midBlockToBack = new HashMap<>();

    public BackModule() {
        backFunctions = new ArrayList<>();
        backGlobalDecls = new ArrayList<>();
    }

    public void addFunction(BackFunction backFunction) {
        backFunctions.add(backFunction);
    }

    public void addGlobalDecl(BackGlobalDecl backGlobalDecl) {
        backGlobalDecls.add(backGlobalDecl);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (BackGlobalDecl backGlobalDecl : backGlobalDecls) {
            sb.append(backGlobalDecl.toString());
        }
        for (BackFunction backFunction : backFunctions) {
            sb.append(backFunction.toString());
        }
        return sb.toString();
    }

    public ArrayList<BackFunction> getFunctions() {
        return backFunctions;
    }

    public void optimize() {
        boolean flag;
        (new Straighten(this)).optimize();
        InstructionMerge instructionMerge = new InstructionMerge(this);
        do {
            flag = instructionMerge.optimize();
        } while (flag);
    }
}
