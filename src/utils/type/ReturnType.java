package src.utils.type;

public enum ReturnType {

    INT,
    VOID,
    FLOAT

}
