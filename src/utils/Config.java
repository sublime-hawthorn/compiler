package src.utils;

public class Config {
    public static boolean output_llvm = false;
    public static boolean opt = false;
    public static String input = "testcase.c";
    public static String output = "testcase.s";
    public static boolean support_mulh = false;
    public static boolean debug = true;
}
