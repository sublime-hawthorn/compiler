package src.mid.Optimizer.RedundancyElim;

import src.mid.IntermediatePresentation.BasicBlock;
import src.mid.IntermediatePresentation.Function.Function;
import src.mid.IntermediatePresentation.Function.MainFunction;
import src.mid.IntermediatePresentation.Function.Param;
import src.mid.IntermediatePresentation.IRManager;
import src.mid.IntermediatePresentation.Instruction.Br;
import src.mid.IntermediatePresentation.Instruction.Call;
import src.mid.IntermediatePresentation.Instruction.Instruction;
import src.mid.IntermediatePresentation.Instruction.Phi;
import src.mid.IntermediatePresentation.Instruction.Ret;
import src.mid.IntermediatePresentation.User;
import src.mid.IntermediatePresentation.Value;
import src.mid.Optimizer.ControllFlow.ControlFlowGraph;
import src.mid.Optimizer.Optimizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class TailRecuElim {
    private final HashMap<Function, HashSet<Call>> tailRecuFuncs = new HashMap<>();

    public void optimize() {
        IRManager.getModule().getDecledFunctions().forEach(this::tailRecuAnalyze);
        tailRecuElim();
    }

    private void tailRecuAnalyze(Function f) {
        /*
           该调用之后，（经过DCE），仅存在br和ret语句；若此函数有返回值，则该返回值一定是本call语句
         */

        if (f instanceof MainFunction) {
            return;
        }
        boolean isVoid = f.isVoid();
        boolean isTailRescu = false;
        HashSet<Call> rescus = new HashSet<>();
        for (BasicBlock b : f.getBlocks()) {
            ArrayList<Instruction> instructions = b.getInstructionList();
            for (int i = 0; i < instructions.size(); i++) {
                Instruction instr = instructions.get(i);
                if (instr instanceof Call call && call.getCallingFunction().equals(f)) {
                    ControlFlowGraph cfg = Optimizer.instance().getCFG();
                    i++;

                    while (i < instructions.size()) {
                        Instruction inst = instructions.get(i);
                        if (!(inst instanceof Ret ret)) {
                            break;
                        } else if (!isVoid && !(ret.getRetValue().equals(call))) {
                            break;
                        }
                        isTailRescu = true;
                        rescus.add(call);
                        i++;
                    }

                    if (cfg.getChildren(b) == null || !isTailRescu) {
                        break;
                    }

                    LinkedList<BasicBlock> queue = new LinkedList<>(cfg.getChildren(b));
                    while (!queue.isEmpty()) {
                        BasicBlock block = queue.poll();
                        if (block.getInstructionList().size() == 1) {
                            Instruction inst = block.getInstructionList().get(0);
                            if (inst instanceof Br) {
                                queue.addAll(cfg.getChildren(block));
                            } else if (inst instanceof Ret ret) {
                                if (!isVoid && !ret.getRetValue().equals(call)) {
                                    break;
                                }
                                isTailRescu = true;
                                rescus.add(call);
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        if (isTailRescu) {
            tailRecuFuncs.put(f, rescus);
        }
    }

    private void tailRecuElim() {
        for (Function f : tailRecuFuncs.keySet()) {
            HashSet<Call> rescus = tailRecuFuncs.get(f);

            BasicBlock newHead = new BasicBlock();
            BasicBlock prevHead = f.getEntranceBlock();
            newHead.addInstruction(new Br(prevHead));
            f.addEntranceBlock(newHead);

            for (int i = 0; i < f.getParam().getParams().size(); i++) {
                Value p = f.getParam().getParams().get(i);
                if (p.isPointer()) {
                    continue;
                }

                Phi phi = new Phi(!p.isFloat(), IRManager.getInstance().declareTempVar());
                for (User user : p.getUserList()) {
                    if (!(user instanceof Param)) {
                        user.replaceOperand(p, phi);
                    }
                }

                phi.addCond(p, newHead);
                for (Call call : rescus) {
                    phi.addCond(call.getOperandList().get(i + 1), call.getBlock());
                }
                prevHead.addInstructionAt(0, phi);
            }

            for (Call call : rescus) {
                Br br = new Br(prevHead);
                call.getBlock().addInstructionAt(
                        call.getBlock().getInstructionList().indexOf(call), br);
                call.getBlock().removeInstruction(call);
            }
        }
    }
}
