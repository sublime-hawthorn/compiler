package src.mid.IntermediatePresentation.Function;

import src.mid.IntermediatePresentation.BasicBlock;
import src.mid.IntermediatePresentation.IRManager;
import src.mid.IntermediatePresentation.Instruction.Instruction;
import src.mid.IntermediatePresentation.ValueType;
public class MainFunction extends Function {
    public MainFunction() {
        super("@main", new Param(), ValueType.I32);
        IRManager.getModule().setMainFunction(this);
    }
}
