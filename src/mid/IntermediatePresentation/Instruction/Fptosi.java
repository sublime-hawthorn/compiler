package src.mid.IntermediatePresentation.Instruction;

import src.mid.IntermediatePresentation.IRManager;
import src.mid.IntermediatePresentation.Value;
import src.mid.IntermediatePresentation.ValueType;

public class Fptosi extends Instruction {
    public Fptosi(Value v) {
        super(IRManager.getInstance().declareTempVar(), ValueType.I32);
        use(v);
    }

    public String toString() {
        return reg + " = fptosi float " + operandList.get(0).getReg() + " to i32\n";
    }
}
