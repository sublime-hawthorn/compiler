package src.mid.IntermediatePresentation.Instruction;

import src.mid.IntermediatePresentation.ConstNumber;
import src.mid.IntermediatePresentation.Function.MainFunction;
import src.mid.IntermediatePresentation.Value;
import src.mid.IntermediatePresentation.ValueType;

public class Ret extends Instruction {

    public Ret() {
        super("RET", ValueType.NULL);
    }

    public Ret(Value retValue, boolean isInt) {
        super("RET", isInt ? ValueType.I32 : ValueType.FLT);
        use(retValue);
    }

    public String toString() {
        if (vType == ValueType.NULL) {
            return "ret void\n";
        } else {
            return "ret " + getTypeString() + " " + operandList.get(0).getReg() + "\n";
        }
    }

    public boolean isUseless() {
        return false;
    }

    public boolean isDefInstr() {
        return false;
    }

    public Value getRetValue() {
        return operandList.get(0);
    }
}
