package src.mid.IntermediatePresentation.Instruction;

import src.mid.IntermediatePresentation.IRManager;
import src.mid.IntermediatePresentation.Value;
import src.mid.IntermediatePresentation.ValueType;

public class Sitofp extends Instruction {
    public Sitofp(Value v) {
        super(IRManager.getInstance().declareTempVar(), ValueType.FLT);
        use(v);
    }

    public String toString() {
        return reg + " = sitofp i32 " + operandList.get(0).getReg() + " to float\n";
    }
}
