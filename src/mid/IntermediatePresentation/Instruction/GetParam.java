package src.mid.IntermediatePresentation.Instruction;

import src.mid.IntermediatePresentation.ValueType;

public class GetParam extends Instruction {
    private final int offset;

    public GetParam(String param, int offset, ValueType valueType) {
        super(param, valueType);
        this.offset = offset;
    }

    public int getOffset() {
        return offset;
    }

    public String toString() {
        return reg + " = getParam " + offset + "\n";
    }
}
